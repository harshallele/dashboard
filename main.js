let sideBarVisible = false;

let expandSidebarBtn = document.getElementById("btn-expand-sidebar");
let sidebar = document.getElementById("sidebar");
let content = document.getElementById("content");

let cardTemplate = "";
let templateLoaded = false;

let dataLoaded = false;
let clientArr = [];

expandSidebarBtn.addEventListener("click",(event) => {
    sideBarVisible = !sideBarVisible;

    if(sideBarVisible){
        sidebar.style.display = "block"
        setTimeout(() => sidebar.style.opacity = "1", 50);
        
    }
    else{
        sidebar.style.opacity = "0";
        setTimeout(() => sidebar.style.display = "none", 1000);
    }
});

if (window.innerWidth > 768){
    sidebar.style.display = "block";
    content.style.marginLeft = "250px";
    sidebar.style.opacity = "1";
}

window.addEventListener("resize",(event) => {
    sidebar.style.opacity = "1";
    if (window.innerWidth >= 768){
        sidebar.style.display = "block";
        content.style.marginLeft = "250px";
    }
    else{
        sidebar.style.display = "none";
        content.style.marginLeft = "0px";
    }    
});

function getCards(){
    return axios.get("card.mustache");
}

function getClients(){
    return axios.get("clients");
}

axios.all([getCards(),getClients()])
    .then(axios.spread((cardResp, clientResp) => {
        
        let cardTemplate = cardResp.data;

        let clients = clientResp.data;
        clients = prepareCardData(clients);

        let cards = document.getElementById("cards");
        
        clients.forEach(element => {
            let renderedText = Mustache.render(cardTemplate,element);
            cards.innerHTML += renderedText;
        });

    }));

function prepareCardData(clientArr){
    clientArr.forEach((element) => {
        element.imgClass = "client-" + element.id;
        let connectionDivs = ""
        
        element.connections.forEach((c) => {
            connectionDivs += "<div class='mx-1 profile-img " + "client-" + c + "'></div>\n";
        });
        
        element.connectionDivs = connectionDivs;
    });

    return clientArr;
}